var ERC20Impl = artifacts.require("./ERC20Impl.sol");
var ERC20Store = artifacts.require("./ERC20Store.sol");

module.exports = function(deployer) {
  deployer.deploy(ERC20Impl, 1000000).then(function() {
    // Token price is 0.001 Ether
    var tokenPrice = 1000000000000000;
    return deployer.deploy(ERC20Store, ERC20Impl.address, tokenPrice);
  });
};
